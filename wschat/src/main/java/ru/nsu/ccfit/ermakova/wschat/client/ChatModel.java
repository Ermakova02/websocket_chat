package ru.nsu.ccfit.ermakova.wschat.client;

import ru.nsu.ccfit.ermakova.wschat.model.Message;
import ru.nsu.ccfit.ermakova.wschat.parser.*;

import javax.swing.*;
import javax.websocket.*;
import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Timer;

public class ChatModel {
    private static final String ERROR_MESSAGE_TITLE = "Ошибка";
    private static final String EMPTY_SERVER_NAME_MESSAGE = "Имя сервера не может быть пустым";
    private static final String EMPTY_USER_NAME_MESSAGE = "Имя пользователя не может быть пустым";
    private static final String MALFORMED_SERVER_NAME_MESSAGE = "Не правильный адрес сервера";
    private static final String HTTP_ERROR_MESSAGE = "Ошибка открытия HTTP соединения";
    private static final String HTTP_PROTOCOL_ERROR_MESSAGE = "Ошибка HTTP протокола";
    private static final String TRANSFER_DATA_ERROR = "Ошибка передачи сообщения";
    private static final String HTTP_ERROR_400 = "Запрос не соответствует формату (400 Bad Request)";
//    private static final String HTTP_ERROR_401 = "В запросе отсутствует токен (401 Unauthorized)";
    private static final String HTTP_ERROR_401 = "Имя пользователя занято (401 Unauthorized)";
    private static final String HTTP_ERROR_403 = "Токен не известен серверу (403 Forbidden)";
    private static final String HTTP_ERROR_405 = "Метод не поддерживается по данному адресу (405 Method Not Allowed)";
    private static final String INCORRECT_SERVER_ANSWER_ERROR = "Не корректный ответ от сервера. Код: ";
    private static final String MALFORMED_SERVER_ANSWER_ERROR = "Ошибочный ответ от сервера";

    private static final int UPDATE_MESSAGES_DELAY = 1000;
    private static final int DEFAULT_PORT = 8080;
    private static final int DEFAULT_WS_PORT = 8081;

    private ChatClient mvc;
    private boolean connected;
    private String address;
    private String userName;
    private String token;
    private int port;
    private int wsport;
    private List<UserData> usersList;
    private int currentMessageId;
//    private MessagesUpdater updater;
    ClientEndpoint wsclient;

    ChatModel(ChatClient mvc) {
        this.mvc = mvc;
        connected = false;
        address = "";
        userName = "";
        port = DEFAULT_PORT;
        wsport = DEFAULT_WS_PORT;
        usersList = new ArrayList<>();
        currentMessageId = 0;
//        updater = new MessagesUpdater(this);
//        updater.setDelay(UPDATE_MESSAGES_DELAY);
        wsclient = null;
    }

    private void standardErrorMessage(String msg) {
        JLabel label = new JLabel(msg);
        label.setFont(mvc.getView().getDefaultFont());
        JOptionPane.showMessageDialog(mvc.getView(), label, ERROR_MESSAGE_TITLE, JOptionPane.ERROR_MESSAGE);
    }

    private boolean parseNewUserAnswer(String response, String user) {
        int index = response.indexOf("\"username\": \"");
        String str = response.substring(index + 13);
        index = str.indexOf('\"');
        str = str.substring(0, index);
        if (!str.equals(user)) return false;
        index = response.indexOf("\"token\": \"");
        if (index == -1) return false;
        str = response.substring(index + 10);
        index = str.indexOf('\"');
        if (index == -1) return false;
        token = str.substring(0, index);
        return true;
    }

    private boolean parseNewMessageAnswer(String response, String msg) {
        return true; // TO DO!!!
    }

    private HttpURLConnection retrieveConnection(String addr) {
        URL url = null;
        try {
            url = new URL(addr);
        } catch (MalformedURLException e) {
//                e.printStackTrace();
            standardErrorMessage(MALFORMED_SERVER_NAME_MESSAGE);
            return null;
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection)url.openConnection();
        } catch (IOException e) {
//                e.printStackTrace();
            standardErrorMessage(HTTP_ERROR_MESSAGE);
            return null;
        }
        return con;
    }

    private boolean setRequestMethod(HttpURLConnection con, String method) {
        try {
            con.setRequestMethod(method);
        } catch (ProtocolException e) {
//                e.printStackTrace();
            standardErrorMessage(HTTP_PROTOCOL_ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    private boolean parseUsersListAnswer(String response) {
        RestParser parser = new RestParser(response);
        usersList = new ArrayList<>();
        try {
            parser.expectChar('{');
            parser.expectParameterSkipSpaces("users");
            parser.expectArrayStart();
            while (true) {
                UserData ud = new UserData(0, "", false);
                parser.expectCharSkipSpaces('{');
                parser.expectParameterSkipSpaces("id");
                parser.goToValue();
                ud.id = parser.getNumber();
                parser.expectChar(',');
                parser.expectParameterSkipSpaces("username");
                parser.goToValue();
                ud.username = parser.getValue();
                parser.expectChar(',');
                parser.expectParameterSkipSpaces("online");
                parser.goToValue();
                ud.online = parser.getBoolean();
                parser.expectCharSkipSpaces('}');
                usersList.add(ud);
                if (!parser.hasNextArrayElement())break;
                parser.expectChar(',');
            }
            parser.expectArrayEnd();
            parser.expectCharSkipSpaces('}');
            if (!parser.endIsReached()) throw new Exception();
        } catch (Exception ex) {
            usersList = new ArrayList<>();
            return false;
        }
        return true;
    }

    private void retrieveUsersList() {
        String addr = address;
        String user = userName;

        addr = "http://" + addr + ":" + Integer.toString(port) + "/users";

        HttpURLConnection con = retrieveConnection(addr);
        if (con == null) return;
        if (!setRequestMethod(con, "GET")) return;
        con.setRequestProperty("Authorization", "Token " + token);

        con.setDoOutput(true);
        con.setDoInput(true);
        StringBuilder response = new StringBuilder();
        try {
            int responseCode = 0;
            responseCode = con.getResponseCode();
            switch (responseCode) {
                case 200:
                    break;
                case 400:
                    standardErrorMessage(HTTP_ERROR_400);
                    return;
                case 401:
                    standardErrorMessage(HTTP_ERROR_401);
                    return;
                case 403:
                    standardErrorMessage(HTTP_ERROR_403);
                    return;
                case 405:
                    standardErrorMessage(HTTP_ERROR_405);
                    return;
                default:
                    standardErrorMessage(INCORRECT_SERVER_ANSWER_ERROR + " (" + Integer.toString(responseCode) + ")");
                    return;
            }
            InputStream is = con.getInputStream();
            int available = 0;
            while ((available = is.available()) != 0) {
                byte [] requestArray = new byte[available];
                int requestLen = is.read(requestArray);
                response.append(new String(requestArray, 0, requestLen));
            }
            is.close();
        } catch (IOException e) {
            standardErrorMessage(TRANSFER_DATA_ERROR);
            return;
        }
        if (!parseUsersListAnswer(response.toString())) {
            standardErrorMessage(MALFORMED_SERVER_ANSWER_ERROR);
            return;
        }
    }

    private void clearHistoryArea() {
        currentMessageId = 0;
        mvc.getView().getHistoryArea().setText(null);
    }

    private boolean parseMessagesList(String response) {
        RestParser parser = new RestParser(response);
        boolean listIsEmpty = false;
        try {
            parser.expectChar('{');
            parser.expectParameterSkipSpaces("messages");
            parser.expectArrayStart();
            while (true) {
                int id;
                String message;
                int userId;
                boolean serverMsg;
                long msgTime;

                StringBuilder outputMessage = null;
                char item = parser.getNextItemSkipSpaces();

                if (item == '{') {
                    // Continue to look for the messages
//                    parser.expectCharSkipSpaces('{');
                    parser.expectParameterSkipSpaces("id");
                    parser.goToValue();
                    id = parser.getNumber();
                    parser.expectChar(',');
                    parser.expectParameterSkipSpaces("message");
                    parser.goToValue();
                    message = parser.getValue();
                    parser.expectChar(',');
                    parser.expectParameterSkipSpaces("author");
                    parser.goToValue();
                    userId = parser.getNumber();
                    parser.expectChar(',');
                    parser.expectParameterSkipSpaces("server");
                    parser.goToValue();
                    serverMsg = parser.getBoolean();
                    parser.expectChar(',');
                    parser.expectParameterSkipSpaces("time");
                    parser.goToValue();
                    msgTime = parser.getLong();
                    parser.expectCharSkipSpaces('}');
                    outputMessage = new StringBuilder();
                    Date date = new Date(msgTime);
                    DateFormat formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    outputMessage.append("[" + formattedDate.format(date) + "]\n");
                    if (!serverMsg) {
                        int index = 0;
                        for ( ; index < usersList.size(); index++)
                            if (usersList.get(index).id == userId) break;
                        if (index == usersList.size()) outputMessage.append("\"UNKNOWN USER\": ");
                        else outputMessage.append("\"" + usersList.get(index).username + "\": ");
                    }
                    outputMessage.append(message + "\n");
                    mvc.getView().getHistoryArea().append(outputMessage.toString());
                    currentMessageId = id;
                    if (!parser.hasNextArrayElement()) break;
                    parser.expectChar(',');
                } else if (item == ']') {
                    // List is empty
                    listIsEmpty = true;
                    break;
                } else throw new Exception();
            }
            if (!listIsEmpty) {
                parser.expectArrayEnd();
            }
            parser.expectCharSkipSpaces('}');
            if (!parser.endIsReached()) throw new Exception();
        } catch (Exception ex) {
            clearHistoryArea();
            return false;
        }
        return (!listIsEmpty);
    }

    private void downloadMessages() {
        int count = 10;
        while (true) {
            String addr = address;

            addr = "http://" + addr + ":" + Integer.toString(port) + "/messages?offset=" + Integer.toString(currentMessageId) + "&count=" + Integer.toString(count);

            HttpURLConnection con = retrieveConnection(addr);
            if (con == null) return;
            if (!setRequestMethod(con, "GET")) return;
            con.setRequestProperty("Authorization", "Token " + token);

            con.setDoOutput(true);
            con.setDoInput(true);
            StringBuilder response = new StringBuilder();
            try {
                int responseCode = 0;
                responseCode = con.getResponseCode();
                switch (responseCode) {
                    case 200:
                        break;
                    case 400:
                        standardErrorMessage(HTTP_ERROR_400);
                        return;
                    case 401:
                        standardErrorMessage(HTTP_ERROR_401);
                        return;
                    case 403:
                        standardErrorMessage(HTTP_ERROR_403);
                        return;
                    case 405:
                        standardErrorMessage(HTTP_ERROR_405);
                        return;
                    default:
                        standardErrorMessage(INCORRECT_SERVER_ANSWER_ERROR + Integer.toString(responseCode));
                        return;
                }
                InputStream is = con.getInputStream();
                int available = 0;
                while ((available = is.available()) != 0) {
                    byte [] requestArray = new byte[available];
                    int requestLen = is.read(requestArray);
                    response.append(new String(requestArray, 0, requestLen));
                }
                is.close();
            } catch (IOException e) {
                standardErrorMessage(TRANSFER_DATA_ERROR);
                return;
            }
            if (!parseMessagesList(response.toString())) break;;
        }
    }

/*    private void stopUpdater() {
        updater.stop();
    }

    private void startUpdater() {
        updater.start();
    }*/

    public void connectButttonPressed() {
        StringBuilder response = new StringBuilder();
        if (connected) {
//            stopUpdater();
            String addr = address;

            addr = "http://" + addr + ":" + Integer.toString(port) + "/logout";

            HttpURLConnection con = retrieveConnection(addr);
            if (con == null) return;
            if (!setRequestMethod(con, "POST")) return;
            con.setRequestProperty("Authorization", "Token " + token);

            con.setDoOutput(true);
            con.setDoInput(true);
            try {
                int responseCode = 0;
                responseCode = con.getResponseCode();
                switch (responseCode) {
                    case 200:
                        break;
                    case 400:
                        standardErrorMessage(HTTP_ERROR_400);
                        return;
                    case 401:
                        standardErrorMessage(HTTP_ERROR_401);
                        return;
                    case 403:
                        standardErrorMessage(HTTP_ERROR_403);
                        return;
                    case 405:
                        standardErrorMessage(HTTP_ERROR_405);
                        return;
                    default:
                        standardErrorMessage(INCORRECT_SERVER_ANSWER_ERROR + Integer.toString(responseCode));
                        return;
                }

                InputStream is = con.getInputStream();
                int available = 0;
                while ((available = is.available()) != 0) {
                    byte [] responseArray = new byte[available];
                    int requestLen = is.read(responseArray);
                    response.append(new String(responseArray, 0, requestLen));
                }
                is.close();
            } catch (IOException e) {
                standardErrorMessage(TRANSFER_DATA_ERROR);
                return;
            }
            disconnectWSServer();
            connected = false;
            userName = "";
            address = "";
            mvc.getView().setConnected(false);
        } else { // Not connected yet
            String addr = mvc.getView().getServerName();
            String user = mvc.getView().getUserName();

            user = user.trim();
            addr = addr.trim();

            if (addr == null | addr.length() == 0) {
                standardErrorMessage(EMPTY_SERVER_NAME_MESSAGE);
                return;
            }

            if (user == null | user.length() == 0) {
                standardErrorMessage(EMPTY_USER_NAME_MESSAGE);
                return;
            }

            addr = "http://" + addr + ":" + Integer.toString(port) + "/login";

            HttpURLConnection con = retrieveConnection(addr);
            if (con == null) return;
            if (!setRequestMethod(con, "POST")) return;
            con.setRequestProperty("Content-Type", "application/json");
            String params = "\r\n" +
                    "{\r\n" +
                    "    \"username\": \"" + user + "\"\r\n" +
                    "}";
            con.setDoOutput(true);
            con.setDoInput(true);
            try {
                OutputStream os = con.getOutputStream();
                os.write(params.getBytes());
                os.flush();
                os.close();
                int responseCode = 0;
                responseCode = con.getResponseCode();
                switch (responseCode) {
                    case 200:
                        break;
                    case 400:
                        standardErrorMessage(HTTP_ERROR_400);
                        return;
                    case 401:
                        standardErrorMessage(HTTP_ERROR_401);
                        return;
                    case 403:
                        standardErrorMessage(HTTP_ERROR_403);
                        return;
                    case 405:
                        standardErrorMessage(HTTP_ERROR_405);
                        return;
                    default:
                        standardErrorMessage(INCORRECT_SERVER_ANSWER_ERROR + Integer.toString(responseCode));
                        return;
                }

                InputStream is = con.getInputStream();
                int available = 0;
                while ((available = is.available()) != 0) {
                    byte [] responseArray = new byte[available];
                    int requestLen = is.read(responseArray);
                    response.append(new String(responseArray, 0, requestLen));
                }
                is.close();
            } catch (IOException e) {
                standardErrorMessage(TRANSFER_DATA_ERROR);
                return;
            }
            if (!parseNewUserAnswer(response.toString(), user)) {
                standardErrorMessage(MALFORMED_SERVER_ANSWER_ERROR);
                return;
            }
            connected = true;
            userName = user;
            address = mvc.getView().getServerName().trim();
            mvc.getView().setConnected(true);
            connectWSServer();
            clearHistoryArea();
            updateMessages();
//            startUpdater();
        }
    }

    private void connectWSServer() {
        wsclient = new ClientEndpoint(mvc, address, wsport);
        wsclient.connect();
        String msg = new String("User " + userName + " logged in");
        try {
            wsclient.getSession().getBasicRemote().sendObject(new Message(true, msg, UUID.fromString(token), userName, 0));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EncodeException e) {
            e.printStackTrace();
        }
    }

    private void disconnectWSServer() {
        String msg = new String("User " + userName + " logged out");
        try {
            wsclient.getSession().getBasicRemote().sendObject(new Message(true, msg, UUID.fromString(token), userName, 0));
            wsclient.getSession().close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EncodeException e) {
            e.printStackTrace();
        }
        wsclient = null;
    }

    public synchronized void moveDownHistoryPane() {
        JScrollBar vsb = mvc.getView().getHistoryPane().getVerticalScrollBar();
        vsb.setValue(vsb.getMaximum());
    }

    public synchronized void updateMessages() {
        retrieveUsersList();
        downloadMessages();
        moveDownHistoryPane();
    }

    public void sendButttonPressed() {
        String msg = mvc.getView().getMessageArea().getText();
        StringBuilder response = new StringBuilder();
        if (msg.length() != 0 & connected) {
            String addr = "http://" + address + ":" + Integer.toString(port) + "/messages";
            HttpURLConnection con = retrieveConnection(addr);
            if (con == null) return;
            if (!setRequestMethod(con, "POST")) return;
            con.setRequestProperty("Authorization", "Token " + token);
            con.setRequestProperty("Content-Type", "application/json");
            String params = "\r\n" +
                    "{\r\n" +
                    "    \"message\": \"" + msg + "\"\r\n" +
                    "}";
            con.setDoOutput(true);
            con.setDoInput(true);
            try {
                OutputStream os = con.getOutputStream();
                os.write(params.getBytes());
                os.flush();
                os.close();
                int responseCode = 0;
                responseCode = con.getResponseCode();
                switch (responseCode) {
                    case 200:
                        break;
                    case 400:
                        standardErrorMessage(HTTP_ERROR_400);
                        return;
                    case 401:
                        standardErrorMessage(HTTP_ERROR_401);
                        return;
                    case 403:
                        standardErrorMessage(HTTP_ERROR_403);
                        return;
                    case 405:
                        standardErrorMessage(HTTP_ERROR_405);
                        return;
                    default:
                        standardErrorMessage(INCORRECT_SERVER_ANSWER_ERROR + Integer.toString(responseCode));
                        return;
                }
                InputStream is = con.getInputStream();
                int available = 0;
                while ((available = is.available()) != 0) {
                    byte [] responseArray = new byte[available];
                    int requestLen = is.read(responseArray);
                    response.append(new String(responseArray, 0, requestLen));
                }
                is.close();
            } catch (IOException e) {
                standardErrorMessage(TRANSFER_DATA_ERROR);
                return;
            }
            if (!parseNewMessageAnswer(response.toString(), msg)) {
                standardErrorMessage(MALFORMED_SERVER_ANSWER_ERROR);
                return;
            }
            try {
                wsclient.getSession().getBasicRemote().sendObject(new Message(false, msg, UUID.fromString(token), userName, 0));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (EncodeException e) {
                e.printStackTrace();
            }
            mvc.getView().getMessageArea().setText(null);
            mvc.getView().getMessageArea().requestFocusInWindow();
//            updateMessages();
//            moveDownHistoryPane();
        }
    }
}
