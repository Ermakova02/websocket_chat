package ru.nsu.ccfit.ermakova.wschat.client;

import java.awt.*;

public class ChatClient {
    private ChatFrame view = null;
    private ChatController controller = null;
    private ChatModel model = null;

    public ChatClient() {
        model = new ChatModel(this);
        view = new ChatFrame(this);
        controller = new ChatController(this);
        view.setVisible(true);
    }

    public ChatController getController() { return controller; }
    public ChatModel getModel() { return model; }
    public ChatFrame getView() { return view;}
}
