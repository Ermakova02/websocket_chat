package ru.nsu.ccfit.ermakova.wschat.model;

import javax.json.Json;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class MessageEncoder implements Encoder.Text<Message> {

    @Override
    public void init(final EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public String encode(final Message message) throws EncodeException {
        String uuid;
        if (message.getSender() == null) uuid = new String("null");
        else uuid = message.getSender().toString();
        return Json.createObjectBuilder()
                .add("servermessage", message.isServerMessage())
                .add("content", message.getContent())
                .add("sender", uuid)
                .add("name", message.getSenderName().toString())
                .add("time", Long.toString(message.getTime()))
                .build().toString();
    }
}
