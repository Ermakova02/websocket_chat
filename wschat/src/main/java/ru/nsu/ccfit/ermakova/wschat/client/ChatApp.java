package ru.nsu.ccfit.ermakova.wschat.client;

import ru.nsu.ccfit.ermakova.wschat.client.*;

import javax.swing.*;

public class ChatApp {
    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ChatClient();
            }
        });
    }
}
