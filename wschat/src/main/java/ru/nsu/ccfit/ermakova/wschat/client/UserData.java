package ru.nsu.ccfit.ermakova.wschat.client;

public class UserData {
    public int id;
    public String username;
    public Boolean online;

    UserData(int id, String username, boolean online) {
        this.id = id;
        this.username = username;
        this.online = online;
    }

    public String toString() {
        return new String("{ " + Integer.toString(id) + ", " + username + ", " + online + " }");
    }
}
