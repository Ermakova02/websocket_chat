package ru.nsu.ccfit.ermakova.wschat.client;

public class MessagesUpdater implements Runnable {
    private static final int DEFAULT_DELAY = 2000;

    private Thread t;
    private boolean started;
    private int delay;
    private ChatModel model = null;

    MessagesUpdater(ChatModel model) {
        this.model = model;
        started = false;
        delay = DEFAULT_DELAY;
        t = new Thread(this, "...");
        t.start();
    }

    public synchronized void setDelay(int delay) {
        this.delay = delay;
    }

    public synchronized void start() {
        started = true;
    }

    public synchronized void stop() {
        started = false;
    }

    public synchronized void interrupt() {
        t.interrupt();
    }

    private synchronized boolean getStarted() {
        return started;
    }

    private synchronized int getDelay() {
        return delay;
    }

    @Override
    public void run() {
        while (!t.isInterrupted()) {
            if (getStarted()) model.updateMessages();
            try {
                Thread.sleep(getDelay());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
