package ru.nsu.ccfit.ermakova.wschat.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChatController {
    private ChatClient mvc;
    ChatController(ChatClient mvc) {
        this.mvc = mvc;
        addEventsProcessing();
    }
    private void addEventsProcessing() {
        mvc.getView().getConnectButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mvc.getModel().connectButttonPressed();
            }
        });
        mvc.getView().getSendButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mvc.getModel().sendButttonPressed();
            }
        });
    }

}
