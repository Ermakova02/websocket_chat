package ru.nsu.ccfit.ermakova.wschat.client;

import javax.swing.*;
import java.awt.*;

public class ChatFrame extends JFrame {
    private static final String DEFAULT_SERVER_NAME = "192.168.43.167";
    private static final String DEFAULT_USER_NAME = "user";
    private static final String SERVER_NAME_LABEL = "Имя сервера:";
    private static final String STATUS_NOT_CONNECTED_LABEL = "Статус: Не подключено";
    private static final String STATUS_CONNECTED_LABEL = "Статус: Подключено";
    private static final String USER_NAME_LABEL = "Имя пользователя:";
    private static final String CONNECT_BUTTON_TEXT = "Подключиться";
    private static final String DISCONNECT_BUTTON_TEXT = "Отключиться";
    private static final String MESSAGES_LABEL = "Сообщения:";
    private static final String SEND_BUTTON_TEXT = "Отправить";
    private static final String MAIN_FRAME_HEAD_TEXT = "Chat Client";
    private static final Dimension MAIN_FRAME_SIZE = new Dimension(830, 802);
    private static final String DEFAULT_FONT_NAME = "Segoe UI";
    private static final int DEFAULT_FONT_SIZE = 12;
//    private static final String MAIN_FRAME_ICON = "ru/nsu/ccfit/ermakova/wschat/client/icons/main.png";
    private static final String MAIN_FRAME_ICON = "icons/main.png";
    private static final Point SERVER_NAME_LABEL_LOCATION = new Point(8,12);
    private static final Dimension SERVER_NAME_LABEL_SIZE = new Dimension(250, 32);
    private static final Point SERVER_NAME_TEXT_LOCATION = new Point(298, 12);
    private static final Dimension SERVER_NAME_TEXT_SIZE = new Dimension(250, 32);
    private static final Point STATUS_LABEL_LOCATION = new Point(556, 12);
    private static final Dimension STATUS_LABEL_SIZE = new Dimension(250, 32);
    private static final Point USER_NAME_LABEL_LOCATION = new Point(8, 56);
    private static final Dimension USER_NAME_LABEL_SIZE = new Dimension(250, 32);
    private static final Point USER_NAME_TEXT_LOCATION = new Point(298, 56);
    private static final Dimension USER_NAME_TEXT_SIZE = new Dimension(250, 32);
    private static final Point CONNECT_BUTTON_LOCATION = new Point(556, 56);
    private static final Dimension CONNECT_BUTTON_SIZE = new Dimension(250, 32);
    private static final Point MESSAGES_LABEL_LOCATION = new Point(8, 100);
    private static final Dimension MESSAGES_LABEL_SIZE = new Dimension(250, 32);
    private static final Point HISTORY_PANE_LOCATION = new Point(8, 144);
    private static final Dimension HISTORY_PANE_SIZE = new Dimension(782+16, 500);
    private static final Point MESSAGE_PANE_LOCATION = new Point(8, 656);
    private static final Dimension MESSAGE_PANE_SIZE = new Dimension(508+32, 96);
    private static final Point SEND_BUTTON_LOCATION = new Point(524+32, 656);
    private static final Dimension SEND_BUTTON_SIZE = new Dimension(250, 96);

    private ChatClient mvc;

    private Font defaultFont;

    JPanel mainPanel;

    private JLabel serverNameLabel;
    private JTextField serverNameText;
    private JLabel statusLabel;
    private JLabel userNameLabel;
    private JTextField userNameText;
    private JButton connectButton;

    private JTextArea historyArea;
    private JLabel messagesLabel;
    private JScrollPane historyPane;
    private JScrollPane messagePane;

    private JTextArea messageArea;
    private JButton sendButton;

    private void addComponents() {
        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        serverNameLabel = new JLabel(SERVER_NAME_LABEL);
        serverNameLabel.setFont(defaultFont);
        serverNameLabel.setLocation(SERVER_NAME_LABEL_LOCATION);
        serverNameLabel.setSize(SERVER_NAME_LABEL_SIZE);

        serverNameText = new JTextField();
        serverNameText.setFont(defaultFont);
        serverNameText.setText(DEFAULT_SERVER_NAME);
        serverNameText.setLocation(SERVER_NAME_TEXT_LOCATION);
        serverNameText.setSize(SERVER_NAME_TEXT_SIZE);

        statusLabel = new JLabel(STATUS_NOT_CONNECTED_LABEL);
        statusLabel.setFont(defaultFont);
        statusLabel.setLocation(STATUS_LABEL_LOCATION);
        statusLabel.setSize(STATUS_LABEL_SIZE);

        userNameLabel = new JLabel(USER_NAME_LABEL);
        userNameLabel.setFont(defaultFont);
        userNameLabel.setLocation(USER_NAME_LABEL_LOCATION);
        userNameLabel.setSize(USER_NAME_LABEL_SIZE);

        userNameText = new JTextField();
        userNameText.setFont(defaultFont);
        userNameText.setText(DEFAULT_USER_NAME);
        userNameText.setLocation(USER_NAME_TEXT_LOCATION);
        userNameText.setSize(USER_NAME_TEXT_SIZE);

        connectButton = new JButton(CONNECT_BUTTON_TEXT);
        connectButton.setFont(defaultFont);
        connectButton.setLocation(CONNECT_BUTTON_LOCATION);
        connectButton.setSize(CONNECT_BUTTON_SIZE);

        messagesLabel = new JLabel(MESSAGES_LABEL);
        messagesLabel.setFont(defaultFont);
        messagesLabel.setLocation(MESSAGES_LABEL_LOCATION);
        messagesLabel.setSize(MESSAGES_LABEL_SIZE);

        historyArea = new JTextArea();
        historyArea.setFont(defaultFont);
        historyArea.setEditable(false);
        historyArea.setLineWrap(true);
        historyArea.setWrapStyleWord(true);
        historyPane = new JScrollPane(historyArea);
        historyPane.setLocation(HISTORY_PANE_LOCATION);
        historyPane.setSize(HISTORY_PANE_SIZE);

        messageArea = new JTextArea();
        messageArea.setFont(defaultFont);
        messageArea.setLineWrap(true);
        messageArea.setWrapStyleWord(true);
        messagePane = new JScrollPane(messageArea);
        messagePane.setLocation(MESSAGE_PANE_LOCATION);
        messagePane.setSize(MESSAGE_PANE_SIZE);

        sendButton = new JButton(SEND_BUTTON_TEXT);
        sendButton.setFont(defaultFont);
        sendButton.setEnabled(false);
        sendButton.setLocation(SEND_BUTTON_LOCATION);
        sendButton.setSize(SEND_BUTTON_SIZE);

        mainPanel.add(serverNameLabel);
        mainPanel.add(serverNameText);
        mainPanel.add(statusLabel);

        mainPanel.add(userNameLabel);
        mainPanel.add(userNameText);
        mainPanel.add(connectButton);

        mainPanel.add(messagesLabel);
        mainPanel.add(historyPane);

        mainPanel.add(messagePane);
        mainPanel.add(sendButton);

        add(mainPanel);
    }

    private void setFrameData() {
        setTitle(MAIN_FRAME_HEAD_TEXT);
        setSize(MAIN_FRAME_SIZE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(MAIN_FRAME_ICON)));
        setLocationRelativeTo(null);
        setResizable(false);
        defaultFont = new Font(ChatFrame.DEFAULT_FONT_NAME, Font.PLAIN, ChatFrame.DEFAULT_FONT_SIZE);
    }

    public JButton getConnectButton() { return connectButton; }
    public JButton getSendButton() { return sendButton; }
    public JTextArea getMessageArea() { return messageArea; }
    public JTextArea getHistoryArea() { return historyArea; }
    public String getServerName() { return serverNameText.getText(); }
    public String getUserName() { return userNameText.getText(); }
    public void setServerName(String name) { serverNameText.setText(name); }
    public void setUserName(String name) { userNameText.setText(name); }
    public JScrollPane getHistoryPane() { return historyPane; }
    public void setConnected(boolean connected) {
        if (connected) {
            statusLabel.setText(STATUS_CONNECTED_LABEL);
            connectButton.setText(DISCONNECT_BUTTON_TEXT);
            sendButton.setEnabled(true);
            sendButton.updateUI();
        } else {
            statusLabel.setText(STATUS_NOT_CONNECTED_LABEL);
            connectButton.setText(CONNECT_BUTTON_TEXT);
            sendButton.setEnabled(false);
            sendButton.updateUI();
        }
    }

    public Font getDefaultFont() { return defaultFont; }

    public ChatFrame(ChatClient mvc) {
        super();
        this.mvc = mvc;
        setFrameData();
        addComponents();
    }
}

