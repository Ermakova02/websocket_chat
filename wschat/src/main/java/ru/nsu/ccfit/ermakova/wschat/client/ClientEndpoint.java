package ru.nsu.ccfit.ermakova.wschat.client;

import org.glassfish.tyrus.client.ClientManager;
import ru.nsu.ccfit.ermakova.wschat.model.Message;
import ru.nsu.ccfit.ermakova.wschat.model.MessageDecoder;
import ru.nsu.ccfit.ermakova.wschat.model.MessageEncoder;

//import static java.lang.String.format;

import javax.websocket.DeploymentException;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import java.net.URI;
import java.net.URISyntaxException;

import static java.lang.String.format;

@javax.websocket.ClientEndpoint(encoders = MessageEncoder.class, decoders = MessageDecoder.class)
public class ClientEndpoint {
    private ChatClient mvc;
    private String address;
    private int port;
    Session session;

    ClientEndpoint(ChatClient mvc, String address, int port) {
        this.mvc = mvc;
        this.address = address;
        this.port = port;
        session = null;
    }

    public void connect() {
        String server = "ws://" + address + ":" + Integer.toString(port) + "/ws/chat";
        ClientManager client = ClientManager.createClient();
        try {
            session = client.connectToServer(/*ClientEndpoint.class*/this, new URI(server));
        } catch (DeploymentException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public Session getSession() { return session; }

    @OnOpen
    public void onOpen(Session session) {
    }

    @OnMessage
    public void onMessage(Message message) {
        mvc.getView().getHistoryArea().append(message.toString());
        mvc.getModel().moveDownHistoryPane();
    }
}
