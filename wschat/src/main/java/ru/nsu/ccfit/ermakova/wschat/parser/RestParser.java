package ru.nsu.ccfit.ermakova.wschat.parser;

public class RestParser {
    private String text;
    private int position;

    public RestParser() {
        position = -1;
        text = new String("");
    }

    public RestParser(String text) {
        this.position = -1;
        this.text = text;
    }

    public void setText(String text) {
        this.text = text;
        position = -1;
    }

    public MethodsEnum parseMethod() throws BadRequestException{
        int index = text.indexOf(' ', position);
        if (index == -1) throw new BadRequestException();
        String str = text.substring(0, index);
        position = index + 1;
        MethodsEnum result = MethodsEnum.fromString(str);
        if (result == null) throw new BadRequestException();
        return result;
    }

    public URIsEnum parseURI() throws BadRequestException {
        int index = position;
        position++;
        while (position < text.length() & text.charAt(position) != '?' & text.charAt(position) != ' ') position++;
        if (position == text.length()) throw new BadRequestException();
        if (text.charAt(position) == '?') position++;
        String str = text.substring(index, position);
        URIsEnum result = URIsEnum.fromString(str);
        if (result == null) throw new BadRequestException();
        return result;
    }

    public int getMessagesOffset() throws BadRequestException {
        int index = text.indexOf("offset", position);
        if (index != position) throw new BadRequestException();
        position += 7;
        StringBuilder sb = new StringBuilder();
        while (position < text.length() & text.charAt(position) >= '0' & text.charAt(position) <= '9') {
            sb.append(text.charAt(position));
            position++;
        }
        position++;
        return Integer.parseInt(sb.toString());
    }

    public int getMessagesCount() throws BadRequestException {
        int index = text.indexOf("count", position);
        if (index != position) throw new BadRequestException();
        position += 6;
        StringBuilder sb = new StringBuilder();
        while (position < text.length() & text.charAt(position) >= '0' & text.charAt(position) <= '9') {
            sb.append(text.charAt(position));
            position++;
        }
        return Integer.parseInt(sb.toString());
    }

    public void moveToJSONData() throws BadRequestException {
        int index = text.indexOf('{', position);
        if (index == -1) throw new BadRequestException();
        position = index + 1;
    }

    public void nextLine () throws BadRequestException {
        while (position < text.length() & text.charAt(position) != '\r' & text.charAt(position) != '\n') position++;
        if (position < text.length() & text.charAt(position) == '\r') position++;
    }

    public void expectChar(char element) throws BadRequestException {
        position++;
        if (position >= text.length()) throw new BadRequestException();
        if (text.charAt(position) != element) throw new BadRequestException();
    }

    public void expectCharSkipSpaces(char element) throws BadRequestException {
        position++;
        if (position >= text.length()) throw new BadRequestException();
        while (position < text.length() & (text.charAt(position) == ' ' | text.charAt(position) == '\r' | text.charAt(position) == '\n')) {
            position++;
        }
        if (position >= text.length()) throw new BadRequestException();
        if (text.charAt(position) != element) throw new BadRequestException();
    }

    public void expectStringSkipSpaces(String str) throws BadRequestException {
        position++;
        if (position >= text.length()) throw new BadRequestException();
        while (position < text.length() & (text.charAt(position) == ' ' | text.charAt(position) == '\r' | text.charAt(position) == '\n')) {
            position++;
        }
        if (position >= text.length()) throw new BadRequestException();
        int index = text.indexOf(str, position);
        if (index == -1) throw new BadRequestException();
        position = index + str.length() - 1;
    }

    public char getNextItemSkipSpaces() throws BadRequestException {
        position++;
        if (position >= text.length()) throw new BadRequestException();
        while (position < text.length() & (text.charAt(position) == ' ' | text.charAt(position) == '\r' | text.charAt(position) == '\n')) {
            position++;
        }
        if (position >= text.length()) throw new BadRequestException();
        return text.charAt(position);
    }

    public void expectParameterSkipSpaces(String parameter) throws BadRequestException {
        position++;
        if (position >= text.length()) throw new BadRequestException();
        while (position < text.length() & (text.charAt(position) == ' ' | text.charAt(position) == '\r' | text.charAt(position) == '\n')) {
            position++;
        }
        if (position >= text.length()) throw new BadRequestException();
        if (text.charAt(position) != '\"') throw new BadRequestException();
        position++;
        StringBuilder sb = new StringBuilder();
        while (position < text.length() & text.charAt(position) != '\"') {
            sb.append(text.charAt(position));
            position++;
        }
        if (position >= text.length()) throw new BadRequestException();
        if (!sb.toString().equals(parameter)) throw new BadRequestException();
    }

    public void expectArrayStart() throws BadRequestException {
        expectChar(':');
        expectChar(' ');
        expectChar('[');
    }

    public void expectArrayEnd() throws BadRequestException {
        expectCharSkipSpaces(']');
    }

    public void goToValue() throws BadRequestException {
        expectChar(':');
        expectChar(' ');
    }

    public int getNumber() throws BadRequestException {
        StringBuilder sb = new StringBuilder();
        position++;
        while (position < text.length() & text.charAt(position) >= '0' & text.charAt(position) <= '9') {
            sb.append(text.charAt(position));
            position++;
        }
        if (sb.length() == 0) throw new BadRequestException();
        int result = Integer.parseInt(sb.toString());
        position--;
        return result;
    }

    public long getLong() throws BadRequestException {
        StringBuilder sb = new StringBuilder();
        position++;
        while (position < text.length() & text.charAt(position) >= '0' & text.charAt(position) <= '9') {
            sb.append(text.charAt(position));
            position++;
        }
        if (sb.length() == 0) throw new BadRequestException();
        long result = Long.parseLong(sb.toString());
        position--;
        return result;
    }

    public Boolean getBoolean() throws BadRequestException {
        position++;
        if (position >= text.length()) throw new BadRequestException();
        int index = text.indexOf("true", position);
        if (index == position) {
            position += 3;
            return true;
        }
        index = text.indexOf("false", position);
        if (index == position) {
            position += 4;
            return false;
        }
        index = text.indexOf("null", position);
        if (index == position) {
            position += 3;
            return null;
        }
        throw new BadRequestException();
    }

    public String getValue() throws BadRequestException {
        StringBuilder sb = new StringBuilder();
        position++;
        if (position >= text.length() | text.charAt(position) != '\"') throw new BadRequestException();
        position++;
        while (position < text.length() & text.charAt(position) != '\"') {
            sb.append(text.charAt(position));
            position++;
        }
        if (position >= text.length()) throw new BadRequestException();
        return sb.toString();
    }

    public String getParameterValue(String parameter) throws BadRequestException {
        expectParameterSkipSpaces(parameter);
        goToValue();
        return getValue();
    }

    public String getTokenData() throws BadRequestException {
        expectStringSkipSpaces("Authorization: Token ");
        if (position >= text.length() + 36) throw new BadRequestException();
        String result = text.substring(position + 1, position + 37);
        position += 36;
        return result;
    }


    public boolean hasNextArrayElement() throws BadRequestException {
        if (position + 1 >= text.length()) throw new BadRequestException();
        return text.charAt(position + 1) == ',';
    }

    public boolean endIsReached() {
        return position == (text.length() - 1);
    }
}
